package testshoptests;

import lib.BrowserFactory;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BookerPlatformScenario {

    protected WebDriver driver;
    protected WebDriverWait myWaitVar;

    @Before
    public void setUp() {
        // Create a new instance of the Chrome driver
        driver = BrowserFactory.createBrowser(BrowserFactory.Browser.FIREFOX);
        myWaitVar = new WebDriverWait(driver,20);
        driver.manage().window().maximize();

        // Open the website
        driver.get("http://192.168.1.121:8080/#/");

        //* add welcome cookie
        Cookie welcomeCookie = new Cookie("welcome", "true");
        driver.manage().addCookie(welcomeCookie);//*/

        driver.navigate().refresh();

    }

    @After
    public void tearDown() {
        // Close the browser
        //driver.quit();
    }
}
