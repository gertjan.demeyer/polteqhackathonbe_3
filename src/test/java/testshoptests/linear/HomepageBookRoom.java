package testshoptests.linear;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import testshoptests.BookerPlatformScenario;

public class HomepageBookRoom extends BookerPlatformScenario {

    @Test
    public void bookARoom() {
        driver.findElement(By.xpath("//button[text()='Book this room']")).click();
        driver.findElement(By.className("room-firstname")).sendKeys("Jens");
        driver.findElement(By.className("room-lastname")).sendKeys("Seronvalle");
        driver.findElement(By.className("room-email")).sendKeys("jens.seronvalle@polteq.com");
        driver.findElement(By.className("room-phone")).sendKeys("012345678912");

        //Element which needs to drag.
        WebElement From=driver.findElement(By.xpath("//*[@class='rbc-date-cell']/a[text()='11']"));

        //Element on which need to drop.
        WebElement To=driver.findElement(By.xpath("//*[@class='rbc-date-cell']/a[text()='15']"));

        //Using Action class for drag and drop.
        Actions act=new Actions(driver);

        //Dragged and dropped.
        act.dragAndDrop(From, To).build().perform();

        driver.findElement(By.xpath("//button[text()='Book']")).click();
    }
}
;