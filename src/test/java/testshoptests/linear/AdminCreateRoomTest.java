package testshoptests.linear;

import org.junit.Test;
import org.openqa.selenium.By;
import testshoptests.BookerPlatformScenario;

public class AdminCreateRoomTest extends BookerPlatformScenario {

    @Test
    public void createRoom() {
        // Skip pop-up
//        driver.findElement(By.id("next")).click();
//        driver.findElement(By.id("next")).click();
//        driver.findElement(By.id("next")).click();
//        driver.findElement(By.id("next")).click();
//        driver.findElement(By.id("closeModal")).click();

        // Open Admin Panel
        driver.findElement(By.xpath("/html/body/div/footer/div/p/a[5]")).click();

        //Username & Password + login
        driver.findElement(By.id("username")).sendKeys("admin");
        driver.findElement(By.id("password")).sendKeys("password");
        driver.findElement(By.id("doLogin")).click();

        driver.findElement(By.id("roomNumber")).sendKeys("666");
        driver.findElement(By.id("roomPrice")).sendKeys("123");
        driver.findElement(By.id("createRoom")).click();

        //Logout
        driver.findElement(By.xpath("/html/body/div/div/nav/div[3]/ul/li[3]/a")).click();
    }

}
