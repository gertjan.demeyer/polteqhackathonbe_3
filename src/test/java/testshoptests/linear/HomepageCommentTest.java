package testshoptests.linear;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import testshoptests.BookerPlatformScenario;

public class HomepageCommentTest extends BookerPlatformScenario {

    @Test
    public void addCommentHomepage() {
        // Skip pop-up
//        driver.findElement(By.id("next")).click();
//        driver.findElement(By.id("next")).click();
//        driver.findElement(By.id("next")).click();
//        driver.findElement(By.id("next")).click();
//        driver.findElement(By.id("closeModal")).click();

        // Enter name
        driver.findElement(By.id("name")).sendKeys("groep 3");

        // Enter mail
        driver.findElement(By.id("email")).sendKeys("groep3@polteq.com");

        // Enter phone
        driver.findElement(By.id("phone")).sendKeys("012345678912");

        // Enter subject
        driver.findElement(By.id("subject")).sendKeys("onderwerp");

        // Enter comment
        driver.findElement(By.id("description")).sendKeys("Dit is een boodschap voor Shady Meadocs B&B");

        // Submit comment
        driver.findElement(By.id("submitContact")).click();

        // Verify that comment was submitted
        Assert.assertEquals("Thanks for getting in touch groep 3!",
                driver.findElement(By.xpath("/html/body/div/div/div/div[5]/div[2]/div/h2")).getText());
    }
}